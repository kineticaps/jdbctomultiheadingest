package com.kinetica.ingest.jdbc.util;

import java.util.List;


public class AvroUtil {

	/**
	 * Builds an Avro schema definition using a JDBC column list and type map
	 * 
	 * @param typeName the string value to use as the name in the type header
	 * @param columnList a sorted map containing the list of column names and JDBC SQL types (int)
	 * @param typeMap a map containing the SQL type to string mapping to use in the Avro schema
	 * @return String the generated Avro schema definition
	 */
	public static String buildAvroSchema(String typeName, List<Column> columnList) {
		StringBuffer sb = new StringBuffer();
		sb.append("{\"type\":\"record\",\"name\":\"");
		sb.append(typeName);
		sb.append("\",\"fields\":[");

		boolean firstColumn = true;
		for (Column column : columnList) {
			if (firstColumn)
			{
				firstColumn = false;
			}
			else
			{
				sb.append(",");
			}
			
			sb.append("{\"name\":\"");
			sb.append(column.name);
			sb.append("\",\"type\":");
			if (column.nullable)
			{
				sb.append("[");
			}
			sb.append("\"");
			sb.append(column.getMappedName());
			sb.append("\"");
			if (column.nullable)
			{
				sb.append(",\"null\"]");
			}
			sb.append("}");
		}
		
		sb.append("]}");
		return sb.toString();
	}
}
