package com.kinetica.ingest.jdbc.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gpudb.protocol.CreateTypeRequest;

public class Column {

	public String name;
	public int sqlType;
	public String typeName;
	public boolean nullable;
	public int precision;
	public int scale;
    
	private static final Map<Integer, String> TYPE_MAPPING;
	static {
		Map<Integer, String> typeMap = new HashMap<Integer, String>();
		typeMap.put(4, "int");
		typeMap.put(-9, "string");
		typeMap.put(12, "string");
		typeMap.put(3, "double");
		typeMap.put(5, "int");
		typeMap.put(1, "string");
		typeMap.put(91, "string"); //Date
		typeMap.put(92, "string"); //Time
		typeMap.put(93, "long"); //Timestamp
		TYPE_MAPPING = typeMap;
	}
	
	public Column(String name, int sqlType, String typeName, boolean nullable, int precision, int scale)
	{
		this.name = name.toLowerCase();
		this.sqlType = sqlType;
		this.typeName = typeName;
		this.nullable = nullable;
		this.precision = precision;
		this.scale = scale;
	}
	
	public String getMappedName()
	{
		return TYPE_MAPPING.get(this.sqlType);
	}
	
	public void addColumnPropsToCreateTypeRequest(Map<String, List<String>> createTypeProperties)
	{
		List<String> propList = new ArrayList<String>();
		
		if (sqlType == 91)
		{
			propList.add(CreateTypeRequest.Properties.DATE);
		}
		else if (sqlType == 92)
		{
			propList.add(CreateTypeRequest.Properties.TIME);
		}
		else if (sqlType == 93)
		{
			propList.add(CreateTypeRequest.Properties.TIMESTAMP);
		}
		else if (!getMappedName().equalsIgnoreCase("string")) {
			//do nothing since the rest of this only applies to string fields
		} else if (precision == 1) {
			propList.add(CreateTypeRequest.Properties.CHAR1);
		} else if (precision == 2) {
			propList.add(CreateTypeRequest.Properties.CHAR2);
		} else if (precision < 5) {
			propList.add(CreateTypeRequest.Properties.CHAR4);
		} else if (precision < 9) {
			propList.add(CreateTypeRequest.Properties.CHAR8);
		} else if (precision < 17) {
			propList.add(CreateTypeRequest.Properties.CHAR16);
		} else if (precision < 33) {
			propList.add(CreateTypeRequest.Properties.CHAR32);
		} else if (precision < 65) {
			propList.add(CreateTypeRequest.Properties.CHAR64);
		} else if (precision < 129) {
			propList.add(CreateTypeRequest.Properties.CHAR128);
		} else if (precision < 257) {
			propList.add(CreateTypeRequest.Properties.CHAR256);
		} else {
			propList.add(CreateTypeRequest.Properties.STORE_ONLY);
		}

		if (propList.size() > 0) {
			createTypeProperties.put(name, propList);
		}
	}
}