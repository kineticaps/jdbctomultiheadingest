package com.kinetica.ingest.jdbc.util;

import java.sql.Date;
import java.util.Calendar;

public class DateUtil {
	
	public static long mergeDateTime(long dateValue, long timeValue)
	{
		Calendar dateCal = Calendar.getInstance();
		dateCal.setTime(new Date(dateValue));
		Calendar timeCal = Calendar.getInstance();
		timeCal.setTime(new Date(timeValue));
		
		dateCal.set(Calendar.HOUR_OF_DAY, timeCal.get(Calendar.HOUR_OF_DAY));
		dateCal.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE));
		dateCal.set(Calendar.SECOND, timeCal.get(Calendar.SECOND));
		
		return dateCal.getTimeInMillis();
	}
}
