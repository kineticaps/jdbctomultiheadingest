package com.kinetica.ingest.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpudb.BulkInserter;
import com.gpudb.GPUdb;
import com.gpudb.GPUdbException;
import com.gpudb.GenericRecord;
import com.gpudb.Type;

public class CopyTableData {
	
	private static final Logger logger = LoggerFactory.getLogger(CopyTableData.class);
	
    private static final String JDBC_SOURCE_URL = "";
    private static final String JDBC_SOURCE_USER = "";
    private static final String JDBC_SOURCE_PWD = "";
    private static final String JDBC_SOURCE_DRIVER_CLASS_NAME = "";
    
	private static final String GPUDB_URL = "";
    private static final String GPUDB_TABLE_NAME = "";
    
    private static final int BATCH_SIZE = 1000;    
	private static final Date LOW_DATE_VALUE = new java.sql.Date(new GregorianCalendar(1000, 0, 1, 0, 0, 0).getTimeInMillis());
    private static final Date HIGH_DATE_VALUE = new java.sql.Date(new GregorianCalendar(2900, 0, 1, 0, 0, 0).getTimeInMillis());
    
    private static GPUdb gpuDB = null;

	public static void main(String[] args) {
		
		logger.debug("Loading JDBC driver");
		try {
			Class.forName(JDBC_SOURCE_DRIVER_CLASS_NAME);
		} catch (ClassNotFoundException e) {
			logger.error("Could not load JDBC driver", e);
			return;
		}
		
		logger.debug("Establishing a database connection to " + GPUDB_URL);
		GPUdb gpudb = getGPUdb();
		
		Connection inputConnection = null;
		Statement stmt = null;
		ResultSet tableSet = null;
		BulkInserter<GenericRecord> bulkInserter = null;
		GenericRecord record = null;
		
		long fetchTime = 0;
		long processTime = 0;
		long loadTime = 0;
		long startTime = 0;
		long stopTime = 0;
		long recordCount = 0;
		
		try {
			logger.debug("Opening database connection");
			inputConnection = DriverManager.getConnection(JDBC_SOURCE_URL, JDBC_SOURCE_USER, JDBC_SOURCE_PWD);
			
			logger.debug("Creating bulk inserter for table " + GPUDB_TABLE_NAME);
			Type dbType = Type.fromTable(gpudb,  GPUDB_TABLE_NAME);
			bulkInserter = new BulkInserter<GenericRecord>(gpudb, GPUDB_TABLE_NAME, dbType, BATCH_SIZE, null);

			logger.debug("Querying data");
			stmt = inputConnection.createStatement();
			tableSet = stmt.executeQuery("SELECT * FROM SCHEMA.TABLE");
			tableSet.setFetchSize(BATCH_SIZE);
			
			logger.debug("Processing result set");
			startTime = System.currentTimeMillis();
			while (tableSet.next())
			{
				recordCount++;
				int columnCount = tableSet.getMetaData().getColumnCount();
				stopTime = System.currentTimeMillis();
				fetchTime += (stopTime - startTime);
				
				startTime = System.currentTimeMillis();
				record = new GenericRecord(dbType);
				for (int i = 1; i <= columnCount; i++) {
					String colName = tableSet.getMetaData().getColumnName(i);
					int colType = tableSet.getMetaData().getColumnType(i);
					String value = tableSet.getString(i);
					
					if (value == null)
					{
						//ignore
					}
					else if (colType == 3)
					{
						record.put(colName, new Double(tableSet.getBigDecimal(i).doubleValue()));
					}
					else if (colType == 5)
					{
						record.put(colName, new Integer(tableSet.getInt(i)));
					}
					else if (colType == 91)
					{
						if (tableSet.getDate(i).before(LOW_DATE_VALUE))
						{
							record.put(colName, LOW_DATE_VALUE.toString());
						}
						else if (tableSet.getDate(i).after(HIGH_DATE_VALUE))
						{
							record.put(colName, HIGH_DATE_VALUE.toString());
						}
						else
						{
							record.put(colName, tableSet.getString(i));
						}
					}
					else if (colType == 92)
					{
						record.put(colName, tableSet.getString(i));
					}
					else if (colType == 93)
					{
						if (tableSet.getDate(i).before(LOW_DATE_VALUE))
						{
							record.put(colName, LOW_DATE_VALUE.getTime());
						}
						else if (tableSet.getDate(i).after(HIGH_DATE_VALUE))
						{
							record.put(colName, HIGH_DATE_VALUE.getTime());
						}
						else
						{
							record.put(colName, new Long(tableSet.getDate(i).getTime()));
						}
					}
					else
					{
						record.put(colName, tableSet.getObject(i));
					}
				}
				stopTime = System.currentTimeMillis();
				processTime += (stopTime - startTime);

				startTime = System.currentTimeMillis();
				bulkInserter.insert(record);
				stopTime = System.currentTimeMillis();
				loadTime += (stopTime - startTime);
				
				if (recordCount % 1000 == 0)
				{
					logger.debug(recordCount + "," + fetchTime + "," + processTime + "," + loadTime);
				}
				
				startTime = System.currentTimeMillis();
			}
			
			logger.debug("Final flush to database");
			bulkInserter.flush();
			
		} catch (SQLException sqlException) {
			logger.error("Could not create JDBC connection", sqlException);
			return;
		} catch (GPUdbException gpudbException) {
			logger.error("Could not write data", gpudbException);
			return;
		} finally {
			try {
				tableSet.close();
			} catch (Exception closeException) {
				logger.warn("Error closing result set", closeException);
			}
			
			try {
				stmt.close();
			} catch (Exception closeException) {
				logger.warn("Error closing statement", closeException);
			}
			
			try {
				inputConnection.close();
			} catch (Exception closeException) {
				logger.warn("Error closing connection", closeException);
			}
		}
		
		logger.debug("End of program");
        System.exit(0);
    }
    
    /**
     * Create a new connection to a GPUdb server.
     * @return An initialized GPUdb Object
     * @throws GPUdbException 
     */
    private static GPUdb getGPUdb()
    {
		if (gpuDB != null)
		{
			return gpuDB;
		}
		
    	try {
			gpuDB = new GPUdb(GPUDB_URL);
		} catch (GPUdbException connectionException) {
			logger.error("Could not establish connection with GPUdb", connectionException);
			System.exit(-1);
		}
        return gpuDB;
    }
}