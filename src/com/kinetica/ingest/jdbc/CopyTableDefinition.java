package com.kinetica.ingest.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpudb.GPUdb;
import com.gpudb.GPUdbException;
import com.gpudb.protocol.CreateTableRequest;
import com.gpudb.protocol.CreateTableResponse;
import com.gpudb.protocol.CreateTypeRequest;
import com.gpudb.protocol.CreateTypeResponse;

import com.kinetica.ingest.jdbc.util.AvroUtil;
import com.kinetica.ingest.jdbc.util.Column;

public class CopyTableDefinition {
	
	private static final Logger logger = LoggerFactory.getLogger(CopyTableDefinition.class);
	
    private static final String JDBC_SOURCE_URL = "";
    private static final String JDBC_SOURCE_USER = "";
    private static final String JDBC_SOURCE_PWD = "";
    private static final String JDBC_SOURCE_DRIVER_CLASS_NAME = "";
    private static final String JDBC_SOURCE_SQL = "SELECT TOP 1 * FROM SCHEMA.TABLE";
    
	private static final String GPUDB_URL = "";
	private static final String GPUDB_COLLECTION_NAME = "";
    private static final String GPUDB_TABLE_NAME = "";
    private static final String GPUDB_TABLE_TYPE_NAME = GPUDB_TABLE_NAME + "_type";
    private static final String GPUDB_TABLE_PRIMARY_KEY_COLUMN = "";
    
    private static GPUdb gpuDB = null;
    
	public static void main(String[] args) {
		
		logger.debug("Loading JDBC driver");
		try {
			Class.forName(JDBC_SOURCE_DRIVER_CLASS_NAME);
		} catch (ClassNotFoundException e) {
			logger.error("Could not load JDBC driver", e);
			return;
		}
		
		Connection inputConnection = null;
		Statement stmt = null;
		ResultSet tableSet = null;
		List<Column> columnList = new ArrayList<Column>();
		
		try {
			logger.debug("Opening database connection");
			inputConnection = DriverManager.getConnection(JDBC_SOURCE_URL, JDBC_SOURCE_USER, JDBC_SOURCE_PWD);			

			logger.debug("Querying data");
			stmt = inputConnection.createStatement();
			stmt.setMaxRows(1);
			tableSet = stmt.executeQuery(JDBC_SOURCE_SQL);
			
			if (tableSet.next())
			{
				logger.debug("Table" + "|" + tableSet.getMetaData().getTableName(1));
				int columnCount = tableSet.getMetaData().getColumnCount();
				for (int i = 1; i <= columnCount; i++) {
					String colName = tableSet.getMetaData().getColumnName(i);
					int colType = tableSet.getMetaData().getColumnType(i);
					String typeName = tableSet.getMetaData().getColumnTypeName(i);
					int isNullable = tableSet.getMetaData().isNullable(i);
					int precision = tableSet.getMetaData().getPrecision(i);
					int scale = tableSet.getMetaData().getScale(i);
					String value = tableSet.getString(i);
					logger.debug("Column" + "|" + colName + "|" + colType + "|" + typeName + "|" + value);
					
					columnList.add(new Column(colName, colType, typeName, (isNullable > 0), precision, scale));
				}
			}
			
		} catch (SQLException sqlException) {
			logger.error("Could not create JDBC connection", sqlException);
			return;
		} finally {
			try {
				tableSet.close();
			} catch (Exception closeException) {
				logger.warn("Error closing result set", closeException);
			}
			
			try {
				stmt.close();
			} catch (Exception closeException) {
				logger.warn("Error closing statement", closeException);
			}
			
			try {
				inputConnection.close();
			} catch (Exception closeException) {
				logger.warn("Error closing connection", closeException);
			}
		}
		
		logger.debug("Creating type definition");
		CreateTypeRequest typeRequest = new CreateTypeRequest();
		typeRequest.setLabel(GPUDB_TABLE_TYPE_NAME);

		String typeDefinition = AvroUtil.buildAvroSchema(GPUDB_TABLE_TYPE_NAME, columnList);
		
		typeRequest.setTypeDefinition(typeDefinition);
		logger.debug("Generated type definition:  " + typeRequest.getTypeDefinition());
		
		logger.debug("Setting keys, indexes, and character lengths");
		Map<String, List<String>> createTypeProperties = new HashMap<String,List<String>>();
		for (Column column : columnList) {
			column.addColumnPropsToCreateTypeRequest(createTypeProperties);
		}
		typeRequest.setProperties(createTypeProperties);
		
		logger.debug("Adding primary key column property");
		if (GPUDB_TABLE_PRIMARY_KEY_COLUMN != null)
		{
			List<String> propList = new ArrayList<String>();
			propList.add(CreateTypeRequest.Properties.PRIMARY_KEY);
			createTypeProperties.put(GPUDB_TABLE_PRIMARY_KEY_COLUMN, propList);
		}
				
		typeRequest.setProperties(createTypeProperties);
		
		logger.debug("Setting table-level options");
		Map<String, String> createTableOptions = GPUdb.options(CreateTableRequest.Options.NO_ERROR_IF_EXISTS,
                CreateTableRequest.Options.TRUE, 
                CreateTableRequest.Options.COLLECTION_NAME, 
                GPUDB_COLLECTION_NAME,
                CreateTableRequest.Options.IS_REPLICATED,
                CreateTableRequest.Options.TRUE);
		
		logger.debug("Establishing a database connection to " + GPUDB_URL);
		GPUdb gpudb = getGPUdb();

		logger.debug("Dropping table:  " + GPUDB_TABLE_NAME);
		deleteTable(GPUDB_TABLE_NAME, gpudb);
		
		try {
			CreateTypeResponse typeResponse = gpudb.createType(typeRequest);
			logger.debug("Type " + typeResponse.getTypeId() + " successfully created");
			CreateTableResponse tableResponse = gpudb.createTable(GPUDB_TABLE_NAME, typeResponse.getTypeId(), createTableOptions);
			logger.debug("Table " + tableResponse.getTableName() + " successfully created");
		} catch (Exception e) {
			logger.error("Could not create table in gpudb: " + e.getMessage(), e);
		}

		logger.debug("End of CopyTableDefinition");
        System.exit(0);
    }

    /**
     * Create a new connection to a GPUdb server.
     * @return An initialized GPUdb Object
     * @throws GPUdbException 
     */
    private static GPUdb getGPUdb()
    {
		if (gpuDB != null)
		{
			return gpuDB;
		}
		
    	try {
			gpuDB = new GPUdb(GPUDB_URL);
		} catch (GPUdbException connectionException) {
			logger.error("Could not establish connection with GPUdb", connectionException);
			System.exit(-1);
		}
        return gpuDB;
    }
    
    /**
     * Checks to see if a table exists with the provided name using the provided database
     * connection.  If there is a table with that name, it then drops the table.
     * 
     * @param tableName The name of the table to be dropped
     * @param gpudb An active connection to gpudb
     */
	private static void deleteTable(String tableName, GPUdb gpudb) {
		try {
            if (gpudb.hasTable(tableName, null).getTableExists()) {
                gpudb.clearTable(tableName, "", null);
                logger.info("Deleted table " + tableName);
            }
        } catch (GPUdbException ex) {
            logger.error("Table '" + tableName + "' could not be deleted.");
            System.exit(-2);
        }
	}
}